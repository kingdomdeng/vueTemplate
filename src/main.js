// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import style from './style/style.less'

import App from './App'
import router from './router'

import store from './util/store';
import ajax from './util/axios';
import directive from './util/directive';
import cookie from './util/cookie';
import filtersMixin from './util/filtersMixin';
import pageMixin from "./util/pageMixin";

import JsEncrypt from 'jsencrypt'
require('./util/mock.js')

Vue.use(ElementUI);
Vue.mixin(filtersMixin)
Vue.config.productionTip = false;
Vue.prototype.$ajax = ajax;
Vue.prototype.$jsEncrypt = JsEncrypt;
window.pageMixin = pageMixin;

// 路由拦截
router.beforeEach( (to, form, next) => {
    if (to.meta.requiresAuth) {
        let token = sessionStorage.getItem('token')
        if (token) {
            next()
        } else {
            next({
                path: '/login'
            })
        }
    } else {
        next()
    }
})

/* eslint-disable no-new */
var app = new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>'
})
