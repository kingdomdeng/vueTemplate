/**
 * Created by admin on 2018/7/27 0027.
 */
// 引入mockjs
const Mock = require('mockjs');
// 获取 mock.Random 对象
const Random = Mock.Random;

let test = Mock.mock('/news/index', 'post', {
    'foods|10-50': [{
        'name': "@ctitle(2,10)",
        "img": "@image('600x600',#b7ef7c)",
        "brief": "@csentence(1,50)",
        "price|0-20.0-2": 1,
        "num": 0,
        "minusFlag": true,
        "time": "@time",
        "peisongfei|0-100.0-2": 1,
        "limit|0-50": 1
    }],
});

/*首页列表*/
let index = Mock.mock('/index?pageCurrent=1&pageSize=10', 'get', {
    'data|10': [{
        'project': "@ctitle(2,10)",
        'name': "@ctitle(2,10)",
        'startTime': "@datetime",
        'endTime': "@datetime",
        'warning': "@ctitle(2,10)",
        'useTime': "@datetime",
        'remark': "@ctitle(2,10)",
    }],
    count: 10
});

export default {
    test,
    index
}
