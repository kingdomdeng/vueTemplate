// 全局filters混入
export default {
    filters: {
        formatTime (val) {
            return val.replace(/T/g, ' ').substr(0, 16)
        },
        formatWarning (val) {
            switch (val) {
                // 过期/已完成/警告/危险
                case 0:
                    return '已过期'
                    break
                case 1:
                    return '已完成'
                    break
                case 3:
                    return '警告'
                    break
                case 4:
                    return '危险'
                    break
                default:
                    return '正常'
            }
        }
    },
}