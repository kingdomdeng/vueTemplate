// 组件分页mixin
export default {
    data() {
        return {
            params: {
                pageCurrent: 1,
                pageSize: 10,
            },
            pageTotal: 0,
            pageFunc: ''
        }
    },
    methods: {
        pageSizeChange (val) {
            this.params.pageSize = val
            this.pageFunc()
        },
        pageCurrentChange (val) {
            this.params.pageCurrent = val
            this.pageFunc()
        },
    },
    created: function () {

    }
}