/**
 * Created by admin on 2018/7/31 0031.
 */
import Vue from 'vue'

var drag = Vue.directive('drag', {
    bind: function (el, binding) {
        let div = el;

        div.onmousedown = function (e) {
            if(e.target==this){
                let disX = e.clientX - div.offsetLeft;
                let disY = e.clientY - div.offsetTop;

                div.style.position = 'absolute'
                div.style.width = '100%'
                div.style.zIndex = 10
                div.style.background = 'rgb(225, 243, 216)'

                document.onmousemove = function (e) {
                    div.style.top = e.clientY - disY + 'px';
                    div.style.left = e.clientX - disX + 'px';
                }

                document.onmouseup = function (e) {
                    div.style.position = 'relative'
                    div.style.top = 0
                    div.style.left = 0
                    div.style.zIndex = 1
                    div.style.width = 'auto'
                    div.style.background = '#f0f2f7'
                    document.onmousemove = null;
                    document.onmouseup = null;

                    e.target.parentNode.appendChild(div);
                }
            }
        }
    },
})

var mathThousand = Vue.directive('mathThousand', {
    inserted: function (el, binding) {
        let text = el.value || el.innerHTML,
            arr = [...text].reverse(),
            newArr = [];

        arr.map((item, index, arr) => {
            if (arr.length < 3) {
                return
            }

            if (index !== 0 && index % 3 === 0) {
                newArr.unshift(',')
            }

            newArr.unshift(item)
        })

        if (el.value) {
            el.value = newArr.join('')
        } else {
            el.innerHTML = newArr.join('')
        }

        if (typeof binding.value === 'function') {
            binding.value(binding.expression)
        }

    }
})


export default {
    mathThousand
}
