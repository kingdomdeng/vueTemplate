/**
 * Created by admin on 2018/4/30 0030.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        increment(state) {
            state.count++
        }
    }
})

export default state;
