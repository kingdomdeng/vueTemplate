import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            meta: { requiresAuth: true },
            component: res => require(['@/pages/index'], res)
        },
        {
            path: '/login',
            name: 'login',
            component: res => require(['@/pages/login'], res)
        },
    ]
})
