/**
 * Created by admin on 2018/7/30 0030.
 */
var menu = [
    {
        title: '功能',
        name: '功能',
        child: [
            {
                title: '首页',
                name: 'index',
                path: './home/home'
            },
        ]
    },
    {
        title: '插件',
        name: '插件',
        child: [
            {
                title: '指令',
                name: 'directive',
                path: './directive/directive'
            },
            {
                title: '混入',
                name: 'mixin',
                path: ''
            }
        ]
    }
]

export {
    menu
}
